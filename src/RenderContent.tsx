import React from 'react';

type RenderContentProps = {
    text: string;
    value: number;
}

export const RenderContent = (props: RenderContentProps ) => (
  <div>
    Valore: { props.text }
    <hr/>
    <div>..... bla bla bla....</div>
    </div>
);

export const RenderEmpty = () => <h1>ZERO</h1>;
