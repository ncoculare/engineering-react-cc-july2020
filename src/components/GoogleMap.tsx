import React from 'react';

interface GoogleMapProps {
  city: string;
  zoom: number;
}

export const GoogleMap: React.FC<GoogleMapProps> = ( { city, zoom } ) => {
  const url = `https://maps.googleapis.com/maps/api/staticmap?center=${city}&zoom=${zoom}&size=800x400&key=AIzaSyDSBmiSWV4-AfzaTPNSJhu-awtfBNi9o2k`
  return <div>
    <em>{city}</em>
    <img src={url} alt="" width="100%"/>
  </div>
}
